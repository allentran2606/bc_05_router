import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import HomePage from "./Page/HomePage/HomePage";
import DetailPage from "./Page/DetailPage/DetailPage";
import "antd/dist/reset.css";
import DemoHookPage from "./Page/DemoHookPage/DemoHookPage";
import Header from "./Components/Header/Header";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/detail/:id" component={DetailPage} />
          <Route
            path="/hook"
            render={() => {
              return <DemoHookPage />;
            }}
          />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
