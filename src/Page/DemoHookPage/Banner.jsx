import React, { memo, useEffect } from "react";

function Banner({ title, handleClick }) {
  useEffect(() => {
    return () => {
      console.log(`willUnmount`);
    };
    // willUnmount
  }, []);

  console.log(`banner render`);

  return (
    <div className="p-5 bg-dark text-white mt-5">
      Banner
      <h2>{title}</h2>
      <button className="btn btn-light" onClick={handleClick}>
        Plus like
      </button>
    </div>
  );
}

export default memo(Banner);
