import React, { useEffect, useState } from "react";
import Banner from "./Banner";

export default function DemoHookPage() {
  let [like, setLike] = useState(1);
  let [share, setShare] = useState(1);

  let handlePlusLike = () => {
    setLike(like + 1);
  };

  useEffect(() => {
    // didMount, didUpdate, willUnmout
    console.log(`did mount`);
  }, [like, share]);

  console.log(`render`);

  return (
    <div>
      <h2>DemoHookPage</h2>
      <span className="display-4">{like}</span>
      <button className="btn btn-success" onClick={() => setLike(like + 1)}>
        Plus like
      </button>
      <br />
      <span className="display-4">{share}</span>
      <button className="btn btn-primary" onClick={() => setShare(share + 1)}>
        Plus share
      </button>
      {like < 5 && (
        <Banner title="Hello CyberSoft" handleClick={handlePlusLike} />
      )}
    </div>
  );
}
