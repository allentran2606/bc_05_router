import { Progress } from "antd";
import axios from "axios";
import React, { Component } from "react";
import { createConfigHeader } from "../utils/utils";

export default class DetailPage extends Component {
  state = {
    detail: {},
  };
  componentDidMount() {
    let { id } = this.props.match.params;
    axios({
      url: `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
      method: "GET",
      headers: createConfigHeader(),
    })
      .then((res) => {
        console.log(res);
        this.setState({ detail: res.data.content });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    let { hinhAnh, danhGia } = this.state.detail;
    return (
      <div>
        <img className="w-50" src={hinhAnh} alt="true"></img>
        <Progress
          type="circle"
          percent={danhGia * 10}
          format={(number) => {
            return `${number / 10} điểm`;
          }}
        />
      </div>
    );
  }
}
