import React, { Component } from "react";
import axios from "axios";
import { createConfigHeader } from "../utils/utils";
import MovieItem from "./MovieItem";

export default class HomePage extends Component {
  state = {
    movieArr: [],
  };
  componentDidMount() {
    axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03",
      method: "GET",
      headers: createConfigHeader(),
    })
      .then((res) => {
        console.log(res);
        this.setState({
          movieArr: res.data.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  renderMovieList = () => {
    return this.state.movieArr.map((item, index) => {
      return (
        <div className="col-3 my-2" key={index}>
          <MovieItem movie={item} />
        </div>
      );
    });
  };

  render() {
    return (
      <div className="container py-5">
        <div className="row">{this.renderMovieList()}</div>
      </div>
    );
  }
}
