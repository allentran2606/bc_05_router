import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export default class Header extends Component {
  render() {
    return (
      <div>
        <NavLink to={"/"}>
          <button className="btn btn-warning mx-5">Home</button>
        </NavLink>

        <NavLink to={"/hook"}>
          <button className="btn btn-info mx-5">Demo Hook Page</button>
        </NavLink>
      </div>
    );
  }
}
